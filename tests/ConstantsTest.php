<?php namespace Dorigo\Constants\Test;

use PHPUnit\Framework\TestCase;
use Dorigo\Constants\Constants;

define("WP_WEBROOT", __DIR__);

class ConstantsTest extends TestCase {

    public function testReturnsValidInstance() {
        $constants = Constants::getInstance();

        $this->assertEquals("Dorigo\Constants\Constants", get_class($constants));
    }

    public function testGetConstantIntegers() {
        $constants = Constants::getInstance();

        $integer = $constants->get("testInteger", false);

        $this->assertSame($integer, 1);
        $this->assertNotSame($integer, "1");
    }

    public function testGetConstantStrings() {
        $constants = Constants::getInstance();

        $string = $constants->get("testString", false);

        $this->assertTrue(is_string($string));
        $this->assertSame($string, "string");
    }

    public function testGetConstantDefaults() {
        $constants = Constants::getInstance();

        $notExists = $constants->get("notExists", true);

        $this->assertNotSame($notExists, null);
        $this->assertNotSame($notExists, false);

        $this->assertTrue($notExists);
    }

}