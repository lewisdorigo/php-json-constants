<?php namespace Dorigo\Constants;

use Dorigo\Singleton\Singleton;
use Anvil\FileSystem\FileSystem;

class Constants extends Singleton {
    private $constants = [];
    private $name = "constants.json";

    protected function __construct() {
        $const = [];

        $possible_roots = [
            defined("WP_WEBROOT") ? WP_WEBROOT : null,
            defined("WP_ROOTDIR") ? WP_ROOTDIR : null,
            $_SERVER['DOCUMENT_ROOT'],
        ];

        $possible_roots = array_filter($possible_roots);

        if(class_exists('Anvil\\FileSystem\\FileSystem')) {
            $themeFiles = FileSystem::getThemeDirectories();
            $possible_roots = array_merge($themeFiles, $possible_roots);
        }

        foreach($possible_roots as $root_directory) {

            if(file_exists("{$root_directory}/{$this->name}")) {
                $const = file_get_contents("{$root_directory}/{$this->name}");
                break;
            }

        }

        $this->constants = $const ? json_decode($const) : [];

        $this->handleErrors(json_last_error());
    }

    private function handleErrors(int $error) {
        if($error === JSON_ERROR_NONE) { return; }

        switch($error) {
            case JSON_ERROR_DEPTH:
                throw new \Exception('Maximum stack depth exceeded');
                break;

            case JSON_ERROR_STATE_MISMATCH:
                throw new \Exception('Invalid or malformed JSON');
                break;

            case JSON_ERROR_CTRL_CHAR:
                throw new \Exception('Control character error, possibly incorrectly encoded');
                break;

            case JSON_ERROR_SYNTAX:
                throw new \Exception('Syntax error');
                break;

            case JSON_ERROR_UTF8:
                throw new \Exception('Malformed UTF-8 characters, possibly incorrectly encoded');
                break;

            case JSON_ERROR_RECURSION:
                throw new \Exception('One or more recursive references in the value to be encoded');
                break;

            case JSON_ERROR_INF_OR_NAN:
                throw new \Exception('One or more NAN or INF values in the value to be encoded');
                break;

            case JSON_ERROR_UNSUPPORTED_TYPE:
                throw new \Exception('A value of a type that cannot be encoded was given');
                break;

            case JSON_ERROR_INVALID_PROPERTY_NAME:
                throw new \Exception('A property name that cannot be encoded was given');
                break;

            case JSON_ERROR_UTF16:
                throw new \Exception('Malformed UTF-16 characters, possibly incorrectly encoded');
                break;

            default:
                throw new \Exception('An unknown error occurred');
                break;
        }
    }

    public function get(string $key = null, $default = false) {
        if($key !== null) {
            return isset($this->constants->{$key}) ? $this->constants->{$key} : $default;
        }

        return $this->constants;
    }
}
